import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import { getRandomInt } from './_util';
import { dataset as mockData } from './mockData';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataset: mockData
    };
  }

  handleClickAdd = evt => {
    evt.preventDefault();
    const rdmIndx = getRandomInt(0, 7);
    const newItem = mockData[rdmIndx];
    const dataset = [...this.state.dataset];

    dataset.push(newItem);
    this.setState({ dataset });
  }

  handleClickDelete = indx => evt => {
    evt.preventDefault();
    // [].filter return a new array, this.state remains immutable
    const dataset = this.state.dataset.filter((item, itemIndx) => itemIndx !== indx);
    this.setState({ dataset });
  }

  render() {
    return (
      <div className="App">
        <br />
        <Grid>
          <Row>
            <Col md={4} sm={4}><h3>Name</h3></Col>
            <Col md={4} sm={4}><h3>Age</h3></Col>
            <Col md={4} sm={4}><h3>Country</h3></Col>
            <Col md={12} sm={12}><hr className="hr-col" /></Col>
          </Row>
          {
            this.state.dataset.map((item, indx) =>
              <Card
                key={indx}
                item={item}
                handleClickDelete={this.handleClickDelete(indx)}
              />)
          }
        </Grid>
        <button onClick={this.handleClickAdd}>
          Add Card
        </button>
      </div>
    );
  }
}

const Card = ({ item, handleClickDelete }) => {
  if (!item || !handleClickDelete) {
    return null;
  }

  return (
    <Row>
      <Col md={4} sm={4}>{item.name}</Col>
      <Col md={2} sm={2}>{item.age}</Col>
      <Col md={4} sm={4}>{item.country}</Col>
      <Col md={2} sm={2}>
        <BtnClose handleClick={handleClickDelete} />
      </Col>
      <Col md={12} sm={12}><hr className="hr-col" /></Col>
    </Row>
  )
};

const BtnClose = ({ handleClick }) => handleClick ? (
  <button type="button" className="close" aria-label="Close" onClick={handleClick}>
    <span aria-hidden="true">&times;</span>
  </button>
) : null;

export default App;
